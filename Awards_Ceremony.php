<?php include('includes/main_header.php'); ?>
<div class="bradcam_area bradcam_bg_1">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="bradcam_text text-center">
                    <h1 class="mb-3 font-weight-bold text-teal text-white" style="font-size: 4em"> احتفالية تكريم الرواد </h1>
                </div>
            </div>
        </div>
    </div>
</div>
<section style="padding: 4rem 0;">
    <div class="container">
        <div class="row">
            <div class="venue">
                <h1 style="text-align: center;"> احتفالية تكريم الرواد</h1>
                <div class="venue-detail">
                    <p> هذه الصفحة مخصصة للاحتفالية بتكريم رواد الخليج (النواخذه) التي تهدف الى تأكيد مقولة اللي ما له اول ماله تالي ... لنحتفل جميعا بمن بنوا الامس الجميل لننعم نحن بحاضر اجمل ... لنشارك جميعا في هذا العرس الخليجي لتكريم 300 من رواد الخليج لنقول لهم شكرا لما قدمتموه فقد كنتم ومازلتم رواد الخليج ... </p>
                    <p> هذه الاحتفالية للتكريم ستكون في استضافة الصالة الثقافية التابعة لهيئة البحرين للثقافة والاثار وذلك مساء يوم 15 أكتوبر 2021 ان شاء الله ، ففي هذا الحفل سيتم تكريم 300 شخصية تركت بصامتها في مسار التنمية المستدامة في دول مجلس التعاون الخليجي ... </p>
                    <p> وبهذه المناسبة سيتم منح 3 جوائز نقدية للفائزين في مسابقة النواخذه من خلال التطبيق على Android او IPhone على ان لا يتجاوز عمر المشترك عن عشرين عاما وسيتم السحب بين الحاصلين على اعلى الدرجات في المسابقة علما بانه يمكن تنزيل المسابقة مجانا والاشتراك من خلال اجهزة الهاتف او من خلال الموقع www.newakhtha.com علما بان الفائز الاول سيحصل على جائزة بمقدار 150 دينار بحريني والثاني 100 دينار بحريني والثالث 50 دينار بحريني وسيحصل السبعة الاخرين على شهادات تقدير مع الميدالية الذهبية الخاصة بالمناسبة . </p>
                    <p> ندعو الجميع لمتابعة الاحتفالية سواء بالحضور شخصيا لحفل التكريم في الصالة الثقافية في مملكة البحرين او من خلال البث المباشر الذي سيكون متاحا وسيتم الاعلان عنه عبر وسائل التواصل الاجتماعي .... </p>
                </div>
                <div class="row">
                <div class="col-lg-4 col-sm-12 col-6">
                <img src="img/logo_culture_dep.png" alt="" style="height: 130px;background-color: #fff;padding:5px;">
                </div>
                <div class="col-lg-4 col-sm-12 col-6">
                <img src="img/GKE_Foundation.jpg" alt="" style="height: 130px;">
                </div>
                <div class="col-lg-4 col-sm-12 col-12" style="margin: 10px 0;">
                <img src="img/wp-1.png" alt="" style="height: 130px;width: 310px;background-color: #fff;">
                </div>
                </div>

                <div class="row">
                <div class="col-lg-4 col-sm-12 col-6" >
                <img src="img/logo_bah.jpg" alt="" style="height: 130px;width:150px;">
                </div>
                <div class="col-lg-4 col-sm-12 col-6">
                <img src="img/albaghli_logo.png" alt="" style="height: 130px;">
                </div>
                <div class="col-lg-4 col-sm-12 col-12" style="padding-top:10px;margin:0 auto;">
                <img src="img/kuwait_industries.jpg" alt="" style="height: 130px;">
                </div>
                </div>

                <div class="row" style="margin: 10px 0;">
                <div class="col-lg-4 col-sm-12 col-6">
                <img src="img/Wp-img.png" alt="" style="height: 110px;width:130px">
                </div>
                <div class="col-lg-4 col-sm-12 col-6" style="margin: 0 0px;">
                <img src="img/whatsapp.jpeg" alt="" style="height: 130px;">
                </div>
                <div class="col-lg-4 col-sm-12 col-12" style="padding-top:10px;margin:0 auto;">
                <img src="img/logo0000.jpg" alt="" style="height: 130px;">
                </div>
                </div>

                <div class="row" style="margin: 10px 0;">
                <div class="col-lg-4 col-sm-12 col-6">
                <img src="img/swift_news.jpg" alt="" style="height: 130px;width:150px">
                </div>
                <div class="col-lg-4 col-sm-12 col-6" style="margin: 0 0px;">
                <img src="img/logo_sustainable.jpeg" alt="" style="height:130px">
                </div>
                <div class="col-lg-4 col-sm-12 col-12" style="padding-top:10px;margin:0 auto;">
                <img src="img/Logo_Europe .jpeg" alt="" style="height:120px;width:300px">
                </div>
                </div>
                <!-- <div class="logos" style="margin: 0 auto;">
                    <img src="img/logo_culture_dep.png" alt="" style="height: 130px;background-color: #fff;padding:5px;">
                    <img src="img/wp-1.png" alt="" style="height: 130px;width: 310px;background-color: #fff;">
                    <img src="img/GKE_Foundation.jpg" alt="" style="height: 130px;">
                </div> -->
                <!-- <div class="logos-1">
                    <img src="img/logo_bah.jpg" alt="" style="height: 130px;">
                    <img src="img/Wp-img.png" alt="" style="height: 130px;width: 220px;">
                    <img src="img/kuwait_industries.jpg" alt="" style="height: 130px;">
                </div> -->
                <!-- <div class="logos-1">
                    <img src="img/albaghli_logo.png" alt="" style="height: 130px;">
                    <img src="img/logo0000.jpg" alt="" style="height: 130px;">
                    <img src="img/whatsapp.jpeg" alt="" style="height: 130px;">
                </div> -->
                <!-- <div class="logos-1">
                    <img src="img/swift_news.jpg" alt="" style="height: 130px;">
                    <img src="img/Logo_Europe .jpeg" alt="" style="height:130px">
                    <img src="img/logo_sustainable.jpeg" alt="" style="height:130px">
                 </div> -->
            </div>
        </div>
    </div>
</section>
<?php include('includes/main_footer.php'); ?>