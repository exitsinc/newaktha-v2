<?php include('includes/main_header.php'); ?>
<!-- bradcam_area  -->
<div class="bradcam_area bradcam_bg_1">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="bradcam_text">
                    <!-- <h3>Winners</h3>
                    <p> <a href="index.html">Home</a> / Winners</p> -->
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ bradcam_area  -->
<!-- ================ contact section start ================= -->
<section class="contact-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-8">
                <h2 class="contact-title">Feedback</h2>
            </div>
            <div class="col-lg-2"></div>
        </div>
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-8">
                <p class="contact-title" style="font-size:
                    18px;font-weight:600;">We welcome your feedback
                    and
                    comments regarding the game and questions. We
                    will be happy to read your suggestions and ideas
                    for improvements.
                    <span style="color: black;"> Please add your
                    comments to the board</span>
                </p>
            </div>
            <div class="col-lg-2"></div>
        </div>
        <div class="row">
            <div class="col-lg-7 comment-board">
                <h2 class="mt-1 mb-3 text-center">Comment Board</h2>
                <div class="row" id="comment-data">
                    <div class="col-lg-12 mb-3">
                        <div class="card comments">
                            <div class="card-body">
                                <h5 class="card-title">Loading....</h5>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-lg-12 mb-3">
                        <div class="card comments">
                            <div class="card-body">
                                <h5 class="card-title">Anto</h5>
                                <p class="card-text">Some quick
                                    example text to build on the
                                    card title and make up
                                the bulk of the card's content.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 mb-3">
                        <div class="card comments">
                            <div class="card-body">
                                <h5 class="card-title">Rugved</h5>
                                <p class="card-text">Some quick
                                    example text to build on the
                                    card title and make up
                                the bulk of the card's content.</p>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
            <div class="col-lg-5" style="padding: 50px;">
                <h2 class="mt-1 mb-3 text-center">Your Feedback</h2>
                <form class="form-contact contact_form"
                    method="post" id="contactForm"
                    novalidate="novalidate">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input class="form-control valid"
                                name="name" id="name"
                                type="text"
                                placeholder="Enter your name"
                                required="true">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input class="form-control valid"
                                name="email" id="email"
                                type="email"
                                placeholder="Email">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <textarea class="form-control w-100"
                                name="feedback" id="feedback"
                                cols="30"
                                rows="9"
                                placeholder="Your Feedback"
                                required="true"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mt-3">
                        <button type="submit" class="button
                        button-contactForm boxed-btn"
                        id="submit_comment">Send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<footer class="footer footer_bg_1">
    <div class="footer_top">
    </div>
    <div class="copy-right_text">
        <div class="container">
            <div class="footer_border"></div>
            <div class="row">
                <div class="col-lg-7">
                    <p class="copy_right">
                        <p style="font-size: 18px !important;text-align: right;">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>
                            document.write(new Date().getFullYear());
                            </script>
                            حقوق التأليف والنشر محفوظة .. عبدالحميد عبدالله الرميثي ونرجو الاشارة الى المصدر في حالة الاقتباس
                            <!--    <a style="color: orange;" href="https://colorlib.com" target="_blank"></a> -->
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                        <p style="font-size: 18px !important;text-align: right;"><a href="javascript:void();" style="color:#6b6a6a;">info@newakhtha.com  / aalromaithy@gmail.com </a></p>
                    </p>
                </div>
                <div class="col-lg-5">
                    <div class="text" style="text-align: center;">
                        <h4 style="color: #6b6a6a;">Play For Free
                        On:</h4>
                        
                    </div>
                    <div class="google-image" style="text-align:
                        center;">
                        <a href="https://play.google.com/store"><img src="img/google.png" alt=""
                        style="width:150px;height:68px;"></a>
                        <a href="https://www.apple.com/ios/app-store/"><img src="img/apple.png" alt=""
                        style="width:150px;"></a>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer_end  -->
<!-- Modal -->
<div class="modal fade custom_search_pop" id="exampleModalCenter"
    tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="serch_form">
                <input type="text" placeholder="Search">
                <button type="submit">search</button>
            </div>
        </div>
    </div>
</div>
<!-- JS here -->
<script src="js/vendor/modernizr-3.5.0.min.js"></script>
<script src="js/vendor/jquery-1.12.4.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/isotope.pkgd.min.js"></script>
<script src="js/ajax-form.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/imagesloaded.pkgd.min.js"></script>
<script src="js/scrollIt.js"></script>
<script src="js/jquery.scrollUp.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/nice-select.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/gijgo.min.js"></script>
<!--contact js-->
<script src="js/contact.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="js/jquery.form.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/mail-script.js"></script>
<script src="js/main.js"></script>
<script>
function myFunction() {
var x = document.getElementById("myTopnav");
if (x.className === "topnav") {
x.className += " responsive";
} else {
x.className = "topnav";
}
}
</script>
<script>
$('#datepicker').datepicker({
iconsLibrary: 'fontawesome',
icons: {
rightIcon: '<span class="fa fa-caret-down"></span>'
}
});
$('#datepicker2').datepicker({
iconsLibrary: 'fontawesome',
icons: {
rightIcon: '<span class="fa fa-caret-down"></span>'
}
});
$(document).ready(function () {
function loadComments() {
$.ajax({
type: "GET",
url: "https://bmf.migoistudios.com/api/user/comments",
success: function (response) {
console.log(response);
var comments = response.comments;
var html = "";
for (let index = 0; index < comments.length; index++) {
const c = comments[index];
console.log(c);
var cnt = index + 1;
html += '<div class="col-lg-12 mb-3"><div class="card comments"><div class="card-body"><h5 class="card-title">' + c.name +
    '</h5><p class="card-text">' + c.comment +
    '</p></div></div></div>';
    }
    $('#comment-data').html(html);
    //  <tr class="table-row"><td>1</td><td>Smith</td><td>50</td></tr>
    }
    });
    }
    loadComments();
    $('#submit_comment').on('click', function(e) {
    e.preventDefault();
    $('#feedback').text('Sending Comment')
    $.ajax({
    type: "POST",
    url: "https://bmf.migoistudios.com/api/user/apicomment",
    data: {
    name:$('#name').val(),
    email:$('#email').val(),
    comment:$('#feedback').val(),
    },
    success: function (response) {
    loadComments();
    console.log(response,{
    name:$('#name').val(),
    email:$('#email').val(),
    feedback:$('#feedback').val(),
    });
    $('#name').val()
    $('#email').val()
    $('#feedback').val();
    $('#feedback').text('Send New')
    }
    })
    });
    
    });
    </script>
</body>
</html>