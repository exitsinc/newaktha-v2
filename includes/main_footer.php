<!-- footer_start  -->
<footer class="footer footer_bg_1">
    <div class="footer_top">
    </div>
    <div class="copy-right_text">
        <div class="container">
            <div class="footer_border"></div>
            <div class="row">
                <div class="col-lg-7">
                    <p class="copy_right">
                        <p style="font-size: 18px !important;text-align: right;">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>
                            document.write(new Date().getFullYear());
                            </script>
                            حقوق التأليف والنشر محفوظة .. عبدالحميد عبدالله الرميثي ونرجو الاشارة الى المصدر في حالة الاقتباس
                            <!--    <a style="color: orange;" href="https://colorlib.com" target="_blank"></a> -->
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                        <p style="font-size: 18px !important;text-align: right;"><a href="javascript:void();" style="color:#6b6a6a;">info@newakhtha.com  / aalromaithy@gmail.com </a></p>
                        <!-- <p><a href="privacy-policy.html">Privacy Policy</a></p> -->
                    </p>
                </div>
                <div class="col-lg-5">
                    <div class="text" style="text-align: center;">
                        <h4 style="color: #6b6a6a;">Play For Free
                        On:</h4>
                        
                    </div>
                    <div class="google-image" style="text-align:
                        center;">
                        <a href="javascript:void();"><img src="img/google.png" alt=""
                        style="width:150px;height:68px;"></a>
                        <a href="javascript:void();"><img src="img/apple.png" alt=""
                        style="width:150px;"></a>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer_end  -->
<script>
function myFunction() {
var x = document.getElementById("myTopnav");
if (x.className === "topnav") {
x.className += " responsive";
} else {
x.className = "topnav";
}
// var x1 = document.getElementById("newakhatha-id");
// if(x1.className == "newakhatha-class"){
// x1.className += " responsive-logo1";
// }
// else{
//     x1.className += "newakhatha-class";
// }
}
</script>
<!-- JS here -->
<script src="js/vendor/modernizr-3.5.0.min.js"></script>
<script src="js/vendor/jquery-1.12.4.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/isotope.pkgd.min.js"></script>
<script src="js/ajax-form.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/imagesloaded.pkgd.min.js"></script>
<script src="js/scrollIt.js"></script>
<script src="js/jquery.scrollUp.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/nice-select.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/gijgo.min.js"></script>
<script src="js/slick.min.js"></script>
<!--contact js-->
<script src="js/contact.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="js/jquery.form.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/mail-script.js"></script>
<script src="js/main.js"></script>
</body>
</html>