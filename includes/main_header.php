<!doctype html>
<html class="no-js" lang="zxx">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Newakhtha</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- <link rel="manifest" href="site.webmanifest"> -->
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
        <!-- Place favicon.ico in the root directory -->
        <!-- CSS here -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/owl.carousel.min.css">
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/themify-icons.css">
        <link rel="stylesheet" href="css/nice-select.css">
        <link rel="stylesheet" href="css/flaticon.css">
        <link rel="stylesheet" href="css/gijgo.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/slick.css">
        <link rel="stylesheet" href="css/slicknav.css">
        <link rel="stylesheet"
            href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css">
            <link rel="stylesheet" href="css/style.css">
            <!-- <link rel="stylesheet" href="css/responsive.css"> -->
        </head>
        
        <body>
            <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
            <![endif]-->
            <!-- header-start -->
            
            <script>
            window.onload = function(){
            console.log('audio play');
            document.getElementById('audio').play();
            }
            </script>
            <header>
                <div class="header-area">
                    <div id="sticky-header" class="main-header-area">
                        <div class="container-fluid p-0">
                            <div class="row align-items-center no-gutters"
                                >
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 text-right align-items-center justify-content-end d-flex custom-navbar" style="background-color: #69D2E7;">
                                    <!-- style="background-color: #FF9E9D;" -->
                                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 col-xl-9" >
                                        <div class="topnav" id="myTopnav">
                                            <a href="javascript:void(0);" class="icon" onclick="myFunction()">
                                                <i class="fa fa-bars"></i>
                                            </a>
                                            <a href="./game/index.html" target="_blank">Play Now</a>
                                            <!-- <a href="ebook.php"> Ebook</a> -->
                                            <a href="contact.php"> للتواصل  </a>
                                            <!-- <a href="winners.php">الفائزون </a> -->
                                            <a href="Awards_Ceremony.php"> احتفالية تكريم الرواد  </a>
                                            <a href="Competition.php"> مسابقة النواخذة  </a>
                                            <a href="gallery.php">البوم الصور </a>
                                            <!-- <a href="educators.php">للمعلمين </a> -->
                                            <!-- <a href="parents.php">أولياء الامور </a> -->
                                            <a href="index.php" class="active">الرئيسية </a>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 col-xl-3 d-flex justify-content-center align-items-center" style="background: #FA6900;" class="newakhatha-class" id="newakhatha-id">
                                        <!-- style="background: #3FB8AF;" -->
                                        <div class="newakhtha-logo">
                                            <a href="index.php">
                                                <img src="img/newaktha-logo.png" alt="" />
                                            </a>
                                        </div>
                                        <!-- <a class="navbar-brand ml-auto" href="https://www.free-css.com/free-css-templates">Newakhtha</a>   -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </header>
                    <!-- header-end -->