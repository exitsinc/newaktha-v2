<?php include('includes/main_header.php'); ?>
<!-- bradcam_area  -->
<div class="bradcam_area bradcam_bg_1">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="bradcam_text">
                    <!-- <h3>contact</h3>
                    <p> <a href="index.html">Home</a> / contact</p> -->
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ bradcam_area  -->
<!-- ================ contact section start ================= -->
<section class="contact-section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="contact-title">Download Certificate</h2>
                <div class="col-lg-5" style="padding: 50px;">
                    <form class="form-contact contact_form" method="get" id="certificateform" novalidate="novalidate">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input class="form-control valid" name="name" id="name" type="text"
                                    placeholder="Enter your name" required="true">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input class="form-control valid" name="cert_no" id="cert_no" type="cert_no"
                                    placeholder="Certificate Number">
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <button type="submit" class="button
                            button-contactForm boxed-btn" id="submit_comment">Download</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- ================ contact section end ================= -->
    <!-- footer_start  -->
    <footer class="footer footer_bg_1">
        <div class="footer_top">
        </div>
        <div class="copy-right_text">
            <div class="container">
                <div class="footer_border"></div>
                <div class="row">
                    <div class="col-lg-7">
                        <p class="copy_right">
                            <p style="font-size: 18px !important;text-align: right;">
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                Copyright &copy;<script>
                                document.write(new Date().getFullYear());
                                </script>
                                حقوق التأليف والنشر محفوظة .. عبدالحميد عبدالله الرميثي ونرجو الاشارة الى المصدر في حالة الاقتباس
                                <!--    <a style="color: orange;" href="https://colorlib.com" target="_blank"></a> -->
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            </p>
                            <p style="font-size: 18px !important;text-align: right;"><a href="javascript:void();" style="color:#6b6a6a;">info@newakhtha.com  / aalromaithy@gmail.com </a></p>
                        </p>
                    </div>
                    <div class="col-lg-5">
                        <div class="text" style="text-align: center;">
                            <h4 style="color: #6b6a6a;">Play For Free
                            On:</h4>
                        </div>
                        <div class="google-image" style="text-align:
                            center;">
                            <a href="https://play.google.com/store"><img src="img/google.png" alt=""
                            style="width:150px;height:68px;"></a>
                            <a href="https://www.apple.com/ios/app-store/"><img src="img/apple.png" alt=""
                            style="width:150px;"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer_end  -->
    <!-- Modal -->
    <div class="modal fade custom_search_pop" id="exampleModalCenter" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="serch_form">
                    <input type="text" placeholder="Search">
                    <button type="submit">search</button>
                </div>
            </div>
        </div>
    </div>
    <!-- JS here -->
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/ajax-form.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/scrollIt.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/nice-select.min.js"></script>
    <script src="js/jquery.slicknav.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/gijgo.min.js"></script>
    <!--contact js-->
    <script src="js/contact.js"></script>
    <script src="js/jquery.ajaxchimp.min.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/mail-script.js"></script>
    <script src="js/main.js"></script>
    <script>
    $('#datepicker').datepicker({
    iconsLibrary: 'fontawesome',
    icons: {
    rightIcon: '<span class="fa fa-caret-down"></span>'
    }
    });
    $('#datepicker2').datepicker({
    iconsLibrary: 'fontawesome',
    icons: {
    rightIcon: '<span class="fa fa-caret-down"></span>'
    }
    });
    </script>
</body>
</html>