<?php include('includes/main_header.php'); ?>
<!-- slider_area_start -->
<!-- <audio id="player"></audio> -->
<audio autoplay id="audio" src="./audio/WhatsApp Audio 2020-08-28 at 4.14.27 PM.mpeg"></audio>
<div class="container">
    <div class="book row d-flex justify-content-center align-items-center">
        <div class="col-md-3" style="overflow: hidden;">
            <a href="ebook.php#London">
                <img src="img/book.jpg" style="width:100%;" alt="">
            </a>
        </div>
        <div class="col-md-3" style="overflow: hidden;">
            <h1 style="font-weight: 600;opacity: 0.8">EBOOK</h1>
        </div>
    </div>
</div>
<div class="slider_area">
    <div class="slider_active owl-carousel">
        <div class="single_slider d-flex align-items-center
            slider_bg_1 overlay">
            <div class="container">
                <div class="row align-items-center d-flex justify-content-end">
                    <div class="col-xl-6 col-md-8">
                        <div class="slider_text slider_text-custom">
                            <span></span>
                            <!-- <h3>Newakhtha</h3> -->
                            <div class="newakhtha-logo">
                                <a href="index.php">
                                    <img src="img/Newakhtha-logo.jpeg" alt="" />
                                </a>
                            </div>
                            <ul>

                                <p>
                                    <li>
                                        <p>حياكم الله في موقع نواخذه .... كلمة نواخذه هي جمع كلمة نوخذه وهي مصطلح كان ومازال مستخدما في منطقة الخليج العربي وتعني ربان او قبطان السفينة ... ونحن نستخدمها هنا بالمعنى الرمزي لتعني القادة فالنوخذه فعلا هو القائد او الرئيس التنفيذي او المدير العام في السفينة حسب مصطلحات اليوم .</p>
                                    </li>
                                    <li>
                                        <p>هذا الموقع مرتبط بمسابقة تحت عنوان ( نواخذه ) وهي مسابقة تعليمية ثقافية تحتوي على 125 سؤال في الاصدار الاول وكل سؤال حول اسم من اسماء رواد الخليج ، بعضهم قد رحل عنا والبعض الاخر أطال الله في اعمارهم مستمرين في العطاء ... الهدف هو تعريف ابناء هذا الجيل باسماء واعمال العمالقة الكبار الذي بنوا ما نراه اليوم وتأكيدا للمثل الشعبي (اللي ماله اول ماله تالي)</p>
                                    </li>
                                </p>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- slider_area_end -->
<!-- about_area_start  -->
<section style="padding: 5%;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h4>
                    ندعوكم للتعرف على كتاب "النواخذه" الذي صدر مؤخرا في جزئين وهو كتاب تعريفي وتوثيقي يحتوي على معلومات وتعريفات عن اكثر من 300 شخصية خليجية من الرواد الذين صنعوا حاضرنا اليوم، والكتاب هو محاولة لتقديم الشكر لهم من ناحية وفي نفس الوقت تعريف هذا الجيل بتلك الشخصيات التي تركت بصمتها في الأدب والفن والرياضة والتجارة والاعمال والاقتصاد وكافة مناحي التنمية.
                    يمكن التعرف على شخصيات الكتاب من خلال زيارة صفحة البوم الصور في الموقع كما يمكن الحصول على الكتاب من خلال التواصل عبر الايميل او تعبئة النموذج في صفحة التواصل.
                    لقد راعينا قدر الامكان البحث عن الصور التي ليس بها قيود حماية حقوق الملكية ونرجو ان نكون قد وفقنا فان لم ننجح في ذلك فاننا نعتذر مسبقا ان كنا قد خالفنا هذا فاننا نلتمس العذر وعلى استعداد للاشارة الى حقوق ملكية اية صورة ويسعدنا الاشارة الى حقوق الملكية لاية صورة علما بان غرض الاستخدام هنا تعليمي وثقافي بحث.
                    نتوق للتواصل مع الجميع لسماع الاراء والانطباعات والمقترحات سواء عن الموقع بشكل عام او المسابقة او الكتاب، كما نرحب باية اسئلة او استفسارات حيث يمكنكم التواصل مباشرة على الايميل aalromaithy@gmail.com
                    مع كل الحب والتقدير ....
                </h4>
            </div>
        </div>
    </div>
</section>
<section class="youtube-video-section">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="home-youtube-video">
                    <iframe class="ifram-class" src="https://www.youtube.com/embed/zCUtgZ5Tn9g?list=RDUWKdIHAkEPY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="home-youtube-video">
                    <iframe class="ifram-class" src="https://www.youtube.com/embed/re1ZfMn6hCE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="home-youtube-video">
                    <iframe class="ifram-class" src="https://www.youtube.com/embed/iTnDYINyUB8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="home-youtube-video">
                    <iframe class="ifram-class" src="https://www.youtube.com/embed/lklEY1D4ZRs?list=RDlCEjY_mBHnQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="home-youtube-video">
                    <iframe class="ifram-class" src="https://www.youtube.com/embed/D28cg1QLbdI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="home-youtube-video">
                    <iframe class="ifram-class" src="https://www.youtube.com/embed/KY9Nntp_5fA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="home-youtube-video">
                    <iframe class="ifram-class" src="https://www.youtube.com/embed/XmVab05bP7w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="home-youtube-video">
                    <iframe class="ifram-class" src="https://www.youtube.com/embed/qUSsU4cObhs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="home-youtube-video">
                    <iframe class="ifram-class" src="https://www.youtube.com/embed/rWYP0uC-FF0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="home-youtube-video">
                    <iframe class="ifram-class" src="https://www.youtube.com/embed/X2Ywip_e-K0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="home-youtube-video">
                    <iframe class="ifram-class" src="https://www.youtube.com/embed/h5LKIuPTzdI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="home-youtube-video">
                    <iframe class="ifram-class" src="https://www.youtube.com/embed/6o2r-6L6pTY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="home-youtube-video">
                    <iframe class="ifram-class" src="https://www.youtube.com/embed/sl_b8tleOtU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="home-youtube-video">
                    <iframe class="ifram-class" src="https://www.youtube.com/embed/76LE55gc1n8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="home-youtube-video">
                    <iframe class="ifram-class" src="https://www.youtube.com/embed/EE4BTIeydQ8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="home-youtube-video">
                    <iframe class="ifram-class" src="https://www.youtube.com/embed/rOPbitLtmLM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="home-youtube-video">
                    <iframe class="ifram-class" src="https://www.youtube.com/embed/dJu3fu0hxso" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="home-youtube-video">
                    <iframe class="ifram-class" src="https://www.youtube.com/embed/Pv8fJzgALps" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>

        </div>
    </div>
</section>

<?php include('includes/main_footer.php'); ?>