<?php include('includes/main_header.php'); ?>
<!-- header-end -->
<!-- bradcam_area  -->
<div class="bradcam_area bradcam_bg_1">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="bradcam_text text-center">
                    <h1 class="mb-3 font-weight-bold text-teal text-white" style="font-size: 4em">للمعلمين</h1>
                    <!-- <h3>Parents</h3>
                    <p> <a href="index.html">Home</a> / parents</p> -->
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ bradcam_area  -->
<!-- about_area_start  -->
<!-- <div class="about_area">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div class="about_exp d-flex align-items-center justify-content-center">
                    <div class="educators_img">
                        <img src="img/edu.jpeg" style="width: 100%;height: 400px;"alt="">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="about_info pl-70">
                    <div class="section_title mb-55">
                        <h3>Will You be<br>
                        <span>My Friend?</span></h3>
                        <div class="devider">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <div class="info_inner">
                        <p>Is an educational game with the main objective to highlight a number of social values and concepts regarding friendship and building cultural bridges. The game is like a competition of five levels with ten questions in each level. , loving  Through this game we aim to highlight for our children a number of social values and concepts such as;
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
<div class="about_area_3" style="padding-top:60px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div class="about_exp d-flex align-items-center
                    justify-content-center">
                    <div class="educators_img">
                        <img src="img/WhatsApp Image 2020-07-27 at 10.33.13 (1).jpeg" style="width:
                        100%;height: 400px;" alt="">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="about_info pl-70">
                    <div class="section_title mb-55">
                        <!-- <h3>Will You be<br>
                        <span>My Friend?</span></h3> -->
                        <div class="devider">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <div class="info_inner">
                        <p> نواخذه هي مسابقة تعليمية تستهدف تعريف الابناء بمسيرة الاباء وانجازاتهم في منطقة الخليج العربي والمسابقة تحتوي على 125 سؤالا عن شخصيات خليجية في الرياضة والادب والفن والاعلام والثقافة والادارة منهم من رحل عنا فلهم الرحمة والغفران .. والبعض  بيننا ندعو لهم بالصحة والعافية وان يطيل الله في اعمارهم ليستمروا في العطاء ...
                            المسابقة مكونة من 5 مستويات في كل مستوى 10 اسئلة وللانتقال من مستوى الى آخر يجب الاجابة على 6 او 7 او 8 اسئلة بشكل صحيح والعدد يختلف حسب المستوى، ولكن في حالة عدم اجتياز اي مستوى يمكن اعادة المحاولة وستكون لديه اسئلة جديدة ومختلفة ولكل مستوى محاولتين او ثلاث محاولات .
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="about_area_2">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="about_info pl-50">
                    
                    <div class="info_inner">
                        
                        <ul dir="rtl">
                            <li>مع كل سؤال هناك الاجابة الصحيحة وهي عبارة عن فقرة او فقرتين تعريفيتين عن الشخصية مع صورة لصاحب او صاحبة الشخصية وفي نهاية المسابقة يمكن للفرد ان يسجل بياناته لاستلام شهادة باجتيازه المسابقة كما ان اسمه سيكون مسجلا بين الفائزين سواء على التطبيق او على الموقع الخاص بالمسابقة www.newakhtha.com
                            </li>
                            <li>يمكن استخدام المسابقة ضمن الاطار المدرسي كاحد الادوات لاثراء الحصة التعليمية في اي مادة من المواد او يمكن اعطاء الطلبة واجب مدرسي للاطلاع وجمع معلومات عن بعض الشخصيات التي في المسابقة كما يمكن خلق وادارة حوارات ومناقشات حول الشخصيات مع استعانة الطالب بمعلومات من الاسرة .
                            </li>
                            
                            <li>االمسابقة يمكن ان تستخدم كموضوعات بحث او بالاحرى فتح شهية وخلق فضول لدى الطلبة للبحث وفي ذلك اثراء للثقافة العامة وتحقيق للمقولة (اللي ماله اول ماله تالي)
                            </li>
                            <li>ارجو من الجميع التواصل مباشرة حول اية ملاحظات ومقترحات يمكن الاخذ بها في الاصدار الثاني والذي سيتضمن مجموعة اخرى من الرواد ليصل العدد الى 250 من رواد الخليج قبل نهاية العام 2020.</li>
                        </ul>
                        
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="new_image">
                    <img src="img/WhatsApp Image 2020-07-27 at 10.33.11.jpeg" alt="">
                </div>
            </div>
            <!-- <div class="col-lg-5">
                <div class="about_exp d-flex align-items-center
                    justify-content-center">
                    
                </div>
            </div> -->
        </div>
    </div>
</div>
<!-- about_area_end  -->
<!-- appertment_area_start  -->
<!-- footer_start  -->
<footer class="footer footer_bg_1">
    <div class="footer_top">
    </div>
    <div class="copy-right_text">
        <div class="container">
            <div class="footer_border"></div>
            <div class="row">
                <div class="col-lg-7">
                    <p class="copy_right">
                        <p style="font-size: 18px !important;text-align: right;">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>
                            document.write(new Date().getFullYear());
                            </script>
                            حقوق التأليف والنشر محفوظة .. عبدالحميد عبدالله الرميثي ونرجو الاشارة الى المصدر في حالة الاقتباس
                            <!--    <a style="color: orange;" href="https://colorlib.com" target="_blank"></a> -->
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                        <p style="font-size: 18px !important;text-align: right;"><a href="javascript:void();" style="color:#6b6a6a;">info@newakhtha.com  / aalromaithy@gmail.com </a></p>
                    </p>
                </div>
                <div class="col-lg-5">
                    <div class="text" style="text-align: center;">
                        <h4 style="color: #6b6a6a;">Play For Free
                        On:</h4>
                        
                    </div>
                    <div class="google-image" style="text-align:
                        center;">
                        <a href="javascript:void();"><img src="img/google.png" alt=""
                        style="width:150px;height:68px;"></a>
                        <a href="javascript:void();"><img src="img/apple.png" alt=""
                        style="width:150px;"></a>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer_end  -->
<script>
function myFunction() {
var x = document.getElementById("myTopnav");
if (x.className === "topnav") {
x.className += " responsive";
} else {
x.className = "topnav";
}
}
</script>
<!-- JS here -->
<script src="js/vendor/modernizr-3.5.0.min.js"></script>
<script src="js/vendor/jquery-1.12.4.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/isotope.pkgd.min.js"></script>
<script src="js/ajax-form.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/imagesloaded.pkgd.min.js"></script>
<script src="js/scrollIt.js"></script>
<script src="js/jquery.scrollUp.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/nice-select.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/gijgo.min.js"></script>
<script src="js/slick.min.js"></script>
<!--contact js-->
<script src="js/contact.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="js/jquery.form.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/mail-script.js"></script>
<script src="js/main.js"></script>
</body>
</html>