<?php include('includes/main_header.php'); ?>
<!-- bradcam_area  -->
<div class="bradcam_area bradcam_bg_1">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="bradcam_text text-center">
					<h1 class="mb-3 font-weight-bold text-teal text-white" style="font-size: 4em">أولياء الامور </h1>
					<!-- <h3>Parents</h3>
					<p> <a href="index.html">Home</a> / parents</p> -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Button -->
<!-- Start Align Area -->
<div class="whole-wrap">
	<div class="container box_1170">
		
		<div class="section-top-border">
			<h3 class="mb-30">لأولياء الامور </h3>
			<div class="row">
				<div class="col-lg-7">
					<blockquote class="generic-blockquote" style="font-size: 1.25em;">
						هذه مسابقة تعليمية ثقافية الهدف منها هو تعريف الابناء ببعض الاسماء من الاباء والاجداد الذين ساهموا في بناء ما نتمتع به اليوم لاننا امتداد لهم .. ليس هناك عمر معين فالمسابقة يمكن ان يشترك بها الاطفال والكبار ايضا ..
						تتكون المسابقة من خمس مستويات في كل مستوى عشرة اسئلة ومع كل سؤال وضعنا الاجابة الصحيحة وهي عبارة عن معلومات عن الشخصية موضع السؤال مع صورة ان وجدت.
						المسابقة هي فرصة لكم للتواصل مع الابناء بالاسلوب الذي يفهمونه ويحبونه اكثر وهو التقنية والتطبيقات ... يمكنك ان تخلق منافسة داخلية ضمن الاسرة لمن يحصل على نقاط اكبر ... نحن نعلم ان هناك الكثير من الاسماء غير معروفة لهم وهذه هي فرصتك لشرح المزيد لهم وتعريفهم بهذه الشخصيات ...
						ولنؤكد دوما بان اللي ماله اول ماله تالي ....
						<a href="#">اضغط هنا لتنزيل المسابقة </a>
					</blockquote>
				</div>
				<div class="col-lg-5">
					<div class="parents_img">
						<img src="img/WhatsApp Image 2020-07-27 at 10.33.12 (1).jpeg" style="width: 100%;height: 400px;object-fit: cover;" alt="">
					</div>
				</div>
			</div>
		</div>
		
		
		
	</div>
</div>
<!-- End Align Area -->
<!-- footer_start  -->
<footer class="footer footer_bg_1">
	<div class="footer_top">
	</div>
	<div class="copy-right_text">
		<div class="container">
			<div class="footer_border"></div>
			<div class="row">
				<div class="col-lg-7">
					<p class="copy_right">
						<p style="font-size: 18px !important;text-align: right;">
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
							Copyright &copy;<script>
							document.write(new Date().getFullYear());
							</script>
							حقوق التأليف والنشر محفوظة .. عبدالحميد عبدالله الرميثي ونرجو الاشارة الى المصدر في حالة الاقتباس
							<!--    <a style="color: orange;" href="https://colorlib.com" target="_blank"></a> -->
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						</p>
						<p style="font-size: 18px !important;text-align: right;"><a href="javascript:void();" style="color:#6b6a6a;">info@newakhtha.com  / aalromaithy@gmail.com </a></p>
					</p>
				</div>
				<div class="col-lg-5">
					<div class="text" style="text-align: center;">
						<h4 style="color: #6b6a6a;">Play For Free
						On:</h4>
						
					</div>
					<div class="google-image" style="text-align:
						center;">
						<a href="javascript:void();"><img src="img/google.png" alt=""
						style="width:150px;height:68px;"></a>
						<a href="javascript:void();"><img src="img/apple.png" alt=""
						style="width:150px;"></a>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- footer_end  -->
<!-- Modal -->
<div class="modal fade custom_search_pop" id="exampleModalCenter"
	tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
	aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="serch_form">
				<input type="text" placeholder="Search">
				<button type="submit">search</button>
			</div>
		</div>
	</div>
</div>
<!-- JS here -->
<script src="js/vendor/modernizr-3.5.0.min.js"></script>
<script src="js/vendor/jquery-1.12.4.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/isotope.pkgd.min.js"></script>
<script src="js/ajax-form.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/imagesloaded.pkgd.min.js"></script>
<script src="js/scrollIt.js"></script>
<script src="js/jquery.scrollUp.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/nice-select.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/gijgo.min.js"></script>
<!--contact js-->
<script src="js/contact.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="js/jquery.form.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/mail-script.js"></script>
<script src="js/main.js"></script>
<script>
function myFunction() {
var x = document.getElementById("myTopnav");
if (x.className === "topnav") {
x.className += " responsive";
} else {
x.className = "topnav";
}
}
</script>
<script>
	
$('#datepicker').datepicker({
iconsLibrary: 'fontawesome',
icons: {
rightIcon: '<span class="fa fa-caret-down"></span>'
}
});
$('#datepicker2').datepicker({
iconsLibrary: 'fontawesome',
icons: {
rightIcon: '<span class="fa fa-caret-down"></span>'
}
});
</script>
</body>
</html>