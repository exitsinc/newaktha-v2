<?php include('includes/main_header.php'); ?>
<!-- bradcam_area  -->
<div class="bradcam_area bradcam_bg_1">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="bradcam_text text-center">
                    <h1 class="mb-3 font-weight-bold text-teal text-white" style="font-size: 4em"> للتواصل  </h1>
                    <!-- <h3>Parents</h3>
                    <p> <a href="index.html">Home</a> / parents</p> -->
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ bradcam_area  -->
<!-- ================ contact section start ================= -->
<section class="contact-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">
                <h2 class="contact-title" style="text-align: right;"> للتواصل  </h2>
            </div>
            <div class="col-lg-2"></div>
        </div>
        <div class="row">
            <div class="col-lg-2"></div>
            <!-- <div class="col-xs-12 col-sm-12 col-md-12 col-8"> -->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">
                <p class="contact-title" style="font-weight:600;text-align: right;">
                    نرحب بانطباعاتكم وملاحظاتكم واسئلتكم ويسعدنا ان نتواصل معكم من اجل التطوير والاضافات ...
                    نرجو تعبئة النموذج او التواصل معنا مباشرة بالايميل     aalromaithy@gmail.com
                </p>
            </div>
            <div class="col-lg-2"></div>
        </div>
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-8">
                <form class="form-contact contact_form"
                    action="contact_process.php" method="post"
                    id="contactForm" novalidate="novalidate">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input class="form-control valid"
                                name="name" id="name"
                                type="text"
                                onfocus="this.placeholder= ''"
                                onblur="this.placeholder= 'Enter
                                your name'" placeholder="الاسم ">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <input class="form-control"
                                name="subject" id="subject"
                                type="text"
                                onfocus="this.placeholder= ''"
                                onblur="this.placeholder= 'Enter
                                Subject'" placeholder="البلد ">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input class="form-control valid"
                                name="email" id="email"
                                type="email"
                                onfocus="this.placeholder= ''"
                                onblur="this.placeholder= 'Enter
                                email address'"
                                placeholder="الايميل ">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <textarea class="form-control w-100"
                                name="message" id="message"
                                cols="30" rows="9"
                                onfocus="this.placeholder= ''"
                                onblur="this.placeholder= 'Enter
                                Message'" placeholder="الرسالة "></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mt-3">
                        <button type="submit" class="button
                        button-contactForm boxed-btn">Send</button>
                    </div>
                </form>
            </div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</section>
<!-- ================ contact section end ================= -->
<!-- footer_start  -->
<footer class="footer footer_bg_1">
    <div class="footer_top">
    </div>
    <div class="copy-right_text">
        <div class="container">
            <div class="footer_border"></div>
            <div class="row">
                <div class="col-lg-7">
                    <p class="copy_right">
                        <p style="font-size: 18px !important;text-align: right;">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>
                            document.write(new Date().getFullYear());
                            </script>
                            حقوق التأليف والنشر محفوظة .. عبدالحميد عبدالله الرميثي ونرجو الاشارة الى المصدر في حالة الاقتباس
                            <!--    <a style="color: orange;" href="https://colorlib.com" target="_blank"></a> -->
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                        <p style="font-size: 18px !important;text-align: right;"><a href="javascript:void();" style="color:#6b6a6a;">info@newakhtha.com  / aalromaithy@gmail.com </a></p>
                        <!-- <p><a href="privacy-policy.html">Privacy Policy</a></p> -->
                    </p>
                </div>
                <div class="col-lg-5">
                    <div class="text" style="text-align: center;">
                        <h4 style="color: #6b6a6a;">Play For Free
                        On:</h4>
                        
                    </div>
                    <div class="google-image" style="text-align:
                        center;">
                        <a href="javascript:void();"><img src="img/google.png" alt=""
                        style="width:150px;height:68px;"></a>
                        <a href="javascript:void();"><img src="img/apple.png" alt=""
                        style="width:150px;"></a>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer_end  -->
<!-- Modal -->
<div class="modal fade custom_search_pop" id="exampleModalCenter"
    tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="serch_form">
                <input type="text" placeholder="Search">
                <button type="submit">search</button>
            </div>
        </div>
    </div>
</div>
<!-- JS here -->
<script src="js/vendor/modernizr-3.5.0.min.js"></script>
<script src="js/vendor/jquery-1.12.4.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/isotope.pkgd.min.js"></script>
<script src="js/ajax-form.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/imagesloaded.pkgd.min.js"></script>
<script src="js/scrollIt.js"></script>
<script src="js/jquery.scrollUp.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/nice-select.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/gijgo.min.js"></script>
<!--contact js-->
<script src="js/contact.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="js/jquery.form.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/mail-script.js"></script>
<script src="js/main.js"></script>
<script>
function myFunction() {
var x = document.getElementById("myTopnav");
if (x.className === "topnav") {
x.className += " responsive";
} else {
x.className = "topnav";
}
}
</script>
<script>
$('#datepicker').datepicker({
iconsLibrary: 'fontawesome',
icons: {
rightIcon: '<span class="fa fa-caret-down"></span>'
}
});
$('#datepicker2').datepicker({
iconsLibrary: 'fontawesome',
icons: {
rightIcon: '<span class="fa fa-caret-down"></span>'
}
});
</script>
</body>
</html>