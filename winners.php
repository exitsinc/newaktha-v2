<?php include('includes/main_header.php'); ?>
<!-- header-end -->
<!-- bradcam_area  -->
<div class="bradcam_area bradcam_bg_1">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="bradcam_text text-center">
                    <h1 class="mb-3 font-weight-bold text-teal text-white" style="font-size: 4em;">الفائزون </h1>
                    <!-- <h3>Parents</h3>
                    <p> <a href="index.html">Home</a> / parents</p> -->
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ bradcam_area  -->
<!-- ================ contact section start ================= -->
<section class="contact-section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section_title mb-55"  style="text-align: right;">
                    <h3>الفائزون
                    </h3>
                    <div class="devider">
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="row">
            <table class="winners-table">
                <tr class="table-row-head">
                    <!-- <th>Rank</th> -->
                    <th>الآسم</th>
                    <th>النقاط</th>
                    <th> تنزيل الشهادة</th>
                </tr>
                <tbody id="leader-data">
                    <tr class="table-row">
                        <td colspan="4">Loading...</td>
                    </tr>
                </tbody>
                <!-- <tr class="table-row">
                    <td>1</td>
                    <td>Smith</td>
                    <td>50</td>
                </tr> -->
            </table>
        </div>
    </div>
</section>
<footer class="footer footer_bg_1">
    <div class="footer_top">
    </div>
    <div class="copy-right_text">
        <div class="container">
            <div class="footer_border"></div>
            <div class="row">
                <div class="col-lg-7">
                    <p class="copy_right">
                        <p style="font-size: 18px !important;text-align: right;">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>
                            document.write(new Date().getFullYear());
                            </script>
                            حقوق التأليف والنشر محفوظة .. عبدالحميد عبدالله الرميثي ونرجو الاشارة الى المصدر في حالة الاقتباس
                            <!--    <a style="color: orange;" href="https://colorlib.com" target="_blank"></a> -->
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                        <p style="font-size: 18px !important;text-align: right;"><a href="javascript:void();" style="color:#6b6a6a;">info@newakhtha.com  / aalromaithy@gmail.com </a></p>
                    </p>
                </div>
                <div class="col-lg-5">
                    <div class="text" style="text-align: center;">
                        <h4 style="color: #6b6a6a;">Play For Free
                        On:</h4>
                    </div>
                    <div class="google-image" style="text-align:
                        center;">
                        <a href="javascript:void();"><img src="img/google.png" alt=""
                        style="width:150px;height:68px;"></a>
                        <a href="javascript:void();"><img src="img/apple.png" alt=""
                        style="width:150px;"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer_end  -->
<!-- Modal -->
<div class="modal fade custom_search_pop" id="exampleModalCenter" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="serch_form">
                <input type="text" placeholder="Search">
                <button type="submit">search</button>
            </div>
        </div>
    </div>
</div>
<!-- JS here -->
<script src="js/vendor/modernizr-3.5.0.min.js"></script>
<script src="js/vendor/jquery-1.12.4.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/isotope.pkgd.min.js"></script>
<script src="js/ajax-form.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/imagesloaded.pkgd.min.js"></script>
<script src="js/scrollIt.js"></script>
<script src="js/jquery.scrollUp.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/nice-select.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/gijgo.min.js"></script>
<!--contact js-->
<script src="js/contact.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="js/jquery.form.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/mail-script.js"></script>
<script src="js/main.js"></script>
<script>
function myFunction() {
var x = document.getElementById("myTopnav");
if (x.className === "topnav") {
x.className += " responsive";
} else {
x.className = "topnav";
}
}
</script>
<script>
$('#datepicker').datepicker({
iconsLibrary: 'fontawesome',
icons: {
rightIcon: '<span class="fa fa-caret-down"></span>'
}
});
$('#datepicker2').datepicker({
iconsLibrary: 'fontawesome',
icons: {
rightIcon: '<span class="fa fa-caret-down"></span>'
}
});
$(document).ready(function () {
$.ajax({
type: "GET",
url: "https://pioneers.migoistudios.com/api/user/leaderboard",
success: function (response) {
console.log(response);
var leaderboard = response.leaderboard;
var html = "";
for (let index = 0; index < leaderboard.length; index++) {
const l = leaderboard[index];
console.log(l);
var cnt = index + 1;
html += '<tr class="table-row"><td>' + l.name +
    '</td><td>' + l.points + '</td><td><a href="https://pioneers.migoistudios.com/certificate/' + l.user_id + '" class="btn btn-primary btn-sm" target="_blank">Download Certificate</a></td></tr>'
    }
    $('#leader-data').html(html);
    //  <tr class="table-row"><td>1</td><td>Smith</td><td>50</td></tr>
    }
    });
    });
    </script>
</body>
</html>